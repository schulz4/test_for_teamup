from django.contrib.postgres.fields import ArrayField
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models

from testing.utils import generate_key


class TestCase(models.Model):
    id = models.CharField(primary_key=True, max_length=10, default=generate_key)
    iq_result = models.IntegerField(
        validators=(
            MinValueValidator(0),
            MaxValueValidator(50)
        ),
        null=True
    )
    iq_updated_at = models.DateTimeField(null=True)
    eq_result = ArrayField(
        models.CharField(max_length=1, blank=True),
        default=list,
        blank=True,
        null=True
    )
    eq_updated_at = models.DateTimeField(null=True)
