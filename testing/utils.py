import string
from random import choice

from rest_framework import serializers


def generate_key() -> str:
    characters = string.ascii_lowercase +string.ascii_uppercase
    key = "".join([choice(characters) for _ in range(10)])
    return key


def validate_characters(value):
    ALLOWED_CHARACTERS = {"а", "б", "в", "г", "д"}

    for char in value:
        if char not in ALLOWED_CHARACTERS:
            raise serializers.ValidationError(f"Invalid character: {char}")
