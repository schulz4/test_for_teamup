from django.urls import path

from testing.views import TestViewSet

urlpatterns = [
    path("create-test/", TestViewSet.as_view({"post": "create"})),
    path("test/<str:pk>", TestViewSet.as_view({"get": "retrieve"})),
    path("set-iqresult/<str:pk>", TestViewSet.as_view({"patch": "patch_iq"})),
    path("set-eqresult/<str:pk>", TestViewSet.as_view({"patch": "patch_eq"})),

]
