from drf_spectacular.utils import extend_schema
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet

from .models import TestCase
from .serializers import (
    IQResultSerializer,
    EQResultSerializer,
    TestSerializer,
    TestKeySerializer,
)


class TestViewSet(ViewSet):
    @extend_schema(responses={
        status.HTTP_200_OK: TestKeySerializer
    })
    def create(self, request, *args, **kwargs):
        """Создание теста"""
        obj = TestCase.objects.create()
        serialize = TestKeySerializer(obj)
        return Response(serialize.data, status=status.HTTP_200_OK)

    @extend_schema(responses={
        status.HTTP_200_OK: TestSerializer
    })
    def retrieve(self, request, pk, *args, **kwargs):
        """Информация о тесте"""
        obj = TestCase.objects.get(id=pk)
        serialized = TestSerializer(obj)
        return Response(serialized.data)

    @extend_schema(request={
        "application/json": IQResultSerializer
    })
    @action(detail=True)
    def patch_iq(self, request, pk, *args, **kwargs):
        """Изменить результат iq теста"""

        serialized = IQResultSerializer(data=request.data)
        serialized.is_valid(raise_exception=True)
        instance = TestCase.objects.get(id=pk)

        serialized.update(instance, serialized.validated_data)
        return Response({"message": "success"}, status=200)

    @extend_schema(request={
        "application/json": EQResultSerializer
    })
    @action(detail=True)
    def patch_eq(self, request, pk, *args, **kwargs):
        """Изменить результат eq теста"""

        serialized = EQResultSerializer(data=request.data)
        serialized.is_valid(raise_exception=True)
        instance = TestCase.objects.get(id=pk)

        serialized.update(instance, serialized.validated_data)
        return Response({"message": "success"}, status=200)
