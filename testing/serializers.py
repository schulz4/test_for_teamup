from datetime import datetime

from rest_framework import serializers

from .models import TestCase
from .utils import validate_characters


class TestKeySerializer(serializers.ModelSerializer):

    class Meta:
        model = TestCase
        fields = ("id", )


class IQResultSerializer(serializers.Serializer):

    iq_result = serializers.IntegerField(min_value=0, max_value=50, required=True)
    
    def update(self, instance, validated_data):
        iq_result = validated_data.get("iq_result")

        instance.iq_updated_at = datetime.utcnow()
        instance.iq_result = iq_result
        instance.save()

        return instance


class EQResultSerializer(serializers.ModelSerializer):
    def update(self, instance, validated_data):
        eq_result = validated_data.get("eq_result")

        instance.eq_updated_at = datetime.utcnow()
        instance.eq_result = eq_result
        instance.save()

        return instance

    eq_result = serializers.ListField(
        child=serializers.CharField(max_length=1),
        min_length=5,
        max_length=5,
        validators=[validate_characters]
    )

    class Meta:
        model = TestCase
        fields = ("eq_result", )


class TestSerializer(serializers.ModelSerializer):
    eq_result = serializers.ListField(
        child=serializers.CharField(max_length=1),
        min_length=5,
        max_length=5,
    )

    class Meta:
        model = TestCase
        fields = "__all__"
